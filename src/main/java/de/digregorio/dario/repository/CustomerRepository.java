package de.digregorio.dario.repository;



import de.digregorio.dario.model.Customer;
import org.springframework.data.repository.CrudRepository;

/**
 * Repostiory für Customer
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public interface CustomerRepository extends CrudRepository<Customer, Long>{
    //Weitere Funktionen waren nicht nötig
}