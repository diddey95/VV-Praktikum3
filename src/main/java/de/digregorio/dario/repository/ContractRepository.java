package de.digregorio.dario.repository;


import de.digregorio.dario.model.Contract;
import org.springframework.data.repository.CrudRepository;

/**
 * Repostiory für Contract
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public interface ContractRepository extends CrudRepository<Contract, Long> {
    //Weitere Funktionen waren nicht nötig
}
