package de.digregorio.dario.administration;

import de.digregorio.dario.model.Contract;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Interface für Funktionen des ContractService
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public interface ContractServiceAdministration {

    /**
     * Gibt alle Contracts der Repository zurück
     * @return List<Contract>
     */
    @RequestMapping(
            path = "/contracts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    List<Contract> getAllContracts();

    /**
     * Gibt Contract mit der Id zurück
     * @param id (Contract ID)
     * @return Contract
     */
    @RequestMapping(
            path = "/contracts/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    Contract getContractsById(@PathVariable Long id);

    /**
     * Fügt Contract einem Customer hinzu
     * @param contract Body des Requests
     * @param customerId (Customer ID
     */
    @RequestMapping(
            path = "/customers/{customerId}/contracts",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void addContract(@RequestBody Contract contract, @PathVariable Long customerId);

    /**
     * Aktualisiert Contract mit angegebenen ID
     * @param contractParameter Body des Requests
     * @param contractId (Contract ID)
     */
    @RequestMapping(
            path = "customers/{customerId}/contracts/{contractId}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void updateContract(@RequestBody Contract contractParameter, @PathVariable Long contractId);

    /**
     * Löscht Contract aus Repository
     * @param id (Contract ID)
     * @param customerId (Customer ID)
     */
    @RequestMapping(
            path = "/customers/{customerId}/contracts/{contractId}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void deleteContract(@PathVariable("contractId") Long id, @PathVariable("customerId") Long customerId);
}
