package de.digregorio.dario.administration;

import de.digregorio.dario.model.Address;
import de.digregorio.dario.model.Customer;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Interface für Funktionen des CustomerService
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
public interface CustomerServiceAdministration {

    /**
     * Gibt alle Customers der Repository zurück
     * @return List<Customer>
     */
    @RequestMapping(
            path = "/customers",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    List<Customer> getAllCustomers();

    /**
     * Gibt Customer mit der Id zurück
     * @param id (Costumer ID)
     * @return Customer
     */
    @RequestMapping(
            path = "/customers/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    Customer getCustomerById(@PathVariable Long id);

    /**
     * Fügt Customer der Repository zu
     * @param customer Body des Requests
     */
    @RequestMapping(
            path = "/customers",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void addCustomer(@RequestBody Customer customer);

    /**
     * Aktualisiert Customer mit der angegebenen ID
     * @param customer Body des Requests
     * @param id (Customer ID)
     */
    @RequestMapping(
            path = "/customers/{id}",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void updateCustomer(@RequestBody Customer customer, @PathVariable Long id);

    /**
     * Löscht Customer mit angegebenen ID aus Repository
     * @param id (Customer ID)
     */
    @RequestMapping(
            path = "/customers/{id}",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void deleteCustomer(@PathVariable Long id);

    /**
     * Fügt Address dem Customer mit angegebenen ID
     * @param address Body des Requests
     * @param customerId (Customer ID)
     */
    @RequestMapping(
            path = "/customers/{customerId}/addresses",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void addAddress(@RequestBody Address address, @PathVariable Long customerId);

    /**
     * Aktualisiert Address des Customers
     * @param address Body des Requests
     * @param customerId (Customer ID)
     */
    @RequestMapping(
            path = "/customers/{customerId}/addresses",
            method = RequestMethod.PUT,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void updateAddress(@RequestBody Address address, @PathVariable Long customerId);

    /**
     * Löscht Address vom Customer
     * @param customerId (Customer ID)
     */
    @RequestMapping(
            path = "/customers/{customerId}/addresses",
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    void deleteAddress(@PathVariable Long customerId);
}
