package de.digregorio.dario.model;

import javax.persistence.*;
import java.util.Date;


/**
 * Contract Entität für Datenbank
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
@Entity(name = "Contract")
public class Contract {

    //Generiert automatisch ID
    @Id
    @GeneratedValue
    @Column(name = "contractId")
    private Long contractId;
    @Column(name = "contractType")
    private String type;
    @Column(name = "contractContribution")
    private Long contribution;
    /**
     * Bei jedem update bekommt Contract eine neue Versionszeit
     * Damit es nicht zu einem Lost-Update kommt
     */
    @Version
    private Date version;

    public Contract(String type, Long contribution) {
        this.type = type;
        this.contribution = contribution;
    }

    public Contract() {
    }

    public void setContractId(Long contractId) {
        this.contractId = contractId;
    }

    public Long getContractId() {
        return contractId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Long getContribution() {
        return contribution;
    }

    public void setContribution(Long contribution) {
        this.contribution = contribution;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Contract{" +
                "contractId=" + contractId +
                ", type='" + type + '\'' +
                ", contribution=" + contribution +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contract contract = (Contract) o;

        return contractId.equals(contract.contractId)
                && (type != null ? type.equals(contract.type) : contract.type == null)
                && (contribution != null ? contribution.equals(contract.contribution) : contract.contribution == null);
    }

    @Override
    public int hashCode() {
        int result = contractId.hashCode();
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (contribution != null ? contribution.hashCode() : 0);
        return result;
    }
}
