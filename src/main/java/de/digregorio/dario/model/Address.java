package de.digregorio.dario.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * Address Objekt wird an Customer über Embeddable angebunden
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
@Embeddable
public class Address{

    @Column(name = "street")
    private String street;
    @Column(name = "postcode")
    private Integer postcode;
    @Column(name = "town")
    private String town;

    public Address(String street, Integer postcode, String town) {
        this.street = street;
        this.postcode = postcode;
        this.town = town;
    }

    public Address() {
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getPostcode() {
        return postcode;
    }

    public void setPostcode(Integer postcode) {
        this.postcode = postcode;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    @Override
    public String toString() {
        return "Address{" +
                "street='" + street + '\'' +
                ", postcode=" + postcode +
                ", town='" + town + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return (street != null ? street.equals(address.street) : address.street == null)
                && (postcode != null ? postcode.equals(address.postcode) : address.postcode == null)
                && (town != null ? town.equals(address.town) : address.town == null);
    }

    @Override
    public int hashCode() {
        int result = street != null ? street.hashCode() : 0;
        result = 31 * result + (postcode != null ? postcode.hashCode() : 0);
        result = 31 * result + (town != null ? town.hashCode() : 0);
        return result;
    }
}
