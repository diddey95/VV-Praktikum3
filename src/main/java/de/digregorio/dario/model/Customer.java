package de.digregorio.dario.model;

import javax.persistence.*;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Customer Entität für Datenbank
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
@Entity(name = "Customer")
public class Customer {

    //Generiert automatisch ID
    @Id
    @GeneratedValue
    @Column(name = "customerId")
    private Long customerId;
    @Column(name = "customerName")
    private String name;
    @Column(name = "customerSurname")
    private String surname;
    @Column(name = "customerBirthday")
    private String birthday;
    //Objekt Address ist eingebunden in Customer
    @Embedded
    private Address address;
    /**
     * Bei jedem update bekommt Customer eine neue Versionszeit
     * Damit es nicht zu einem Lost-Update kommt
     */
    @Version
    private Date version;

    /**
     * Customer und Contracts werden zusammengeführt
     */
    @OneToMany()
    @JoinTable(
            name = "customerContract",
            joinColumns = {@JoinColumn(name = "customerId", referencedColumnName = "customerId")},
            inverseJoinColumns = {@JoinColumn(name = "contractId", referencedColumnName = "contractId", unique = true)}
    )
    private List<Contract> contracts = new LinkedList<>();

    Customer() {
    }

    public Customer(String name, String surname, String birthday) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
    }

    public Customer(String name, String surname, String birthday, Address address) {
        this.name = name;
        this.surname = surname;
        this.birthday = birthday;
        this.address = address;
    }


    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public List<Contract> getContracts() {
        return contracts;
    }

    public void setContracts(List<Contract> contracts) {
        this.contracts = contracts;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public Date getVersion() {
        return version;
    }

    public void setVersion(Date version) {
        this.version = version;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthday='" + birthday + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Customer customer = (Customer) o;

        return customerId.equals(customer.customerId)
                && (name != null ? name.equals(customer.name) : customer.name == null)
                && (surname != null ? surname.equals(customer.surname) : customer.surname == null)
                && (birthday != null ? birthday.equals(customer.birthday) : customer.birthday == null);
    }

    @Override
    public int hashCode() {
        int result = customerId.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (surname != null ? surname.hashCode() : 0);
        result = 31 * result + (birthday != null ? birthday.hashCode() : 0);
        return result;
    }
}
