package de.digregorio.dario;

import de.digregorio.dario.model.Address;
import de.digregorio.dario.model.Customer;
import de.digregorio.dario.repository.CustomerRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * Verteilte Verarbeitung SS 2017
 * Praktikum Übung 3
 * RESTful WebService Server
 *
 * Folgende Funktionen hat der Server:
 *  - Spring Boot Server
 *  - Verwaltet Datenstruktur Customer, Address, Contract
 *  - CustomerService und ContractService um DS zu verwalten
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
@SpringBootApplication
public class RestServerApplication {

    @Bean
    InitializingBean seedDatabase(CustomerRepository repo) {
        return () -> {
            repo.save(new Customer("Harry", "Hirsch", "01.07.1995",
                    new Address("Maierstr.24",83022,"München")));
            repo.save(new Customer("Willi", "Winzig", "01.08.1990"));
            repo.save(new Customer("Bernd", "Winzig", "20.09.2000"));
        };
    }
    public static void main(String[] args) {
        SpringApplication.run(RestServerApplication.class, args);
    }
}
