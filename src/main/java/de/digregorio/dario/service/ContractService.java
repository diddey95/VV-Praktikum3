package de.digregorio.dario.service;


import de.digregorio.dario.administration.ContractServiceAdministration;
import de.digregorio.dario.model.Contract;
import de.digregorio.dario.model.Customer;
import de.digregorio.dario.repository.ContractRepository;
import de.digregorio.dario.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * ContractService Klasse um Datenbank
 * mit CRUD-Operationen über HTTP zu manipulieren
 * <p>
 * Methoden sind aus ContracServiceAdministration Interface
 * JavaDoc sind entsprechend dort zufinden
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
@RestController
public class ContractService implements ContractServiceAdministration {
    private final ContractRepository contractRepository;

    private final CustomerRepository customerRepository;

    @Autowired
    public ContractService(ContractRepository contractRepository, CustomerRepository customerRepository) {
        this.contractRepository = contractRepository;
        this.customerRepository = customerRepository;
    }


    @Override
    public List<Contract> getAllContracts() {
        ArrayList<Contract> contracts = new ArrayList<>();
        contractRepository.findAll().forEach(contracts::add);

        return contracts;
    }

    @Override
    public Contract getContractsById(@PathVariable Long id) {
        return contractRepository.findOne(id);
    }

    @Override
    public void addContract(@RequestBody Contract contract, @PathVariable Long customerId) {
        contractRepository.save(contract);
        Customer customer = customerRepository.findOne(customerId);
        customer.getContracts().add(contract);
        customerRepository.save(customer);
    }

    @Override
    public void updateContract(@RequestBody Contract contractParameter, @PathVariable Long contractId) {
        Contract contract = contractRepository.findOne(contractId);
        contract.setContribution(contractParameter.getContribution());
        contract.setType(contractParameter.getType());
        contractRepository.save(contract);
    }

    @Override
    public void deleteContract(@PathVariable("contractId") Long id,
                               @PathVariable("customerId") Long customerId) {
        Contract contract = contractRepository.findOne(id);
        Customer customer = customerRepository.findOne(customerId);

        customer.getContracts().remove(contract);
        customerRepository.save(customer);
        contractRepository.delete(id);
    }
}
