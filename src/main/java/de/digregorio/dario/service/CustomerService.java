package de.digregorio.dario.service;

import de.digregorio.dario.administration.CustomerServiceAdministration;
import de.digregorio.dario.model.Address;
import de.digregorio.dario.model.Customer;
import de.digregorio.dario.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * CustomerService Klasse um Datenbank
 * mit CRUD-Operationen über HTTP zu manipulieren
 *
 * Methoden sind aus CustomerServiceAdministration Interface
 * JavaDoc sind entsprechend dort zufinden
 *
 * @author Dario Digregorio (Matrikelnummer: 853630)
 * @version 1.0
 */
@RestController
public class CustomerService implements CustomerServiceAdministration {
    private final CustomerRepository customerRepository;

    @Autowired
    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public List<Customer> getAllCustomers() {
        ArrayList<Customer> customers = new ArrayList<>();

        Iterable<Customer> query = customerRepository.findAll();
        query.forEach(customers::add);
        return customers;
    }

    @Override
    public Customer getCustomerById(@PathVariable Long id) {
        return customerRepository.findOne(id);
    }

    @Override
    public void addCustomer(@RequestBody Customer customer) {
        customerRepository.save(customer);
    }

    @Override
    public void updateCustomer(@RequestBody Customer customer, @PathVariable Long id) {
        Customer newCustomer = customerRepository.findOne(id);
        newCustomer.setName(customer.getName());
        newCustomer.setSurname(customer.getSurname());
        newCustomer.setBirthday(customer.getBirthday());
        customerRepository.save(newCustomer);
    }

    @Override
    public void deleteCustomer(@PathVariable Long id) {
        customerRepository.delete(id);
    }

    @Override
    public void addAddress(@RequestBody Address address, @PathVariable Long customerId) {
        Customer customer = customerRepository.findOne(customerId);
        customer.setAddress(address);
        customerRepository.save(customer);
    }

    @Override
    public void updateAddress(@RequestBody Address address, @PathVariable Long customerId) {
        Customer customer = customerRepository.findOne(customerId);
        customer.setAddress(address);
        customerRepository.save(customer);
    }

    @Override
    public void deleteAddress(@PathVariable Long customerId) {
        Customer customer = customerRepository.findOne(customerId);
        customer.setAddress(null);
        customerRepository.save(customer);
    }
}
